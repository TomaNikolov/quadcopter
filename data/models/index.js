'use strict';

var User = require('./User');
var Image = require('./Image');
var Video = require('./Video');

module.exports = {
    UserModel: User,
    VideoModel: Video,
    ImageModel: Image
};
